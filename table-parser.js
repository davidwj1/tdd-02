const getHeaderName = (header) => {
  const headerTokens = header.split("|");
  const headerName = headerTokens.filter((element) => element !== "")[0].trim();
  return headerName;
};

const getRowObject = (row, headerName) => {
  const rowTokens = row.split("|");
  const rowName = rowTokens.filter((element) => element !== "")[0].trim();
  const rowObj = { [headerName]: rowName };
  return rowObj;
};

const parse = (table) => {
  if (table !== "") {
    const headersAndRows = table.split("\n");
    const headerName = getHeaderName(headersAndRows[0]);

    if (headersAndRows.length === 2) {
      const headerName = getHeaderName(headersAndRows[0]);
      const rowObject = getRowObject(headersAndRows[1], headerName);
      return { header: [headerName], rows: [rowObject] };
    }
    if(headersAndRows.length === 3) {
        const headerName = getHeaderName(headersAndRows[0]);
        const rowObject1 = getRowObject(headersAndRows[1], headerName);
        const rowObject2 = getRowObject(headersAndRows[2], headerName);
        return { header: [headerName], rows: [rowObject1,rowObject2] };
    }

    return { header: [headerName], rows: [] };
  }
  return { header: [], rows: [] };
};

module.exports = parse;
