const parse = require('./table-parser')

describe('table-parser', () => {
    it('parses a table with no columns and no rows',() =>{
        const table = ``
        expect(parse(table)).toEqual({
            header:[],
            rows:[]
        })
    }),
    it('parses a table with 1 column and 0 rows',() =>{
        const table = `| id |`
        expect(parse(table)).toEqual({
            header:["id"],
            rows:[]
        })
    }),
    it('parses a table with 1 columns and 1 rows', () => {
        const table = 
`| id |
| 1 |`
        expect(parse(table)).toEqual({
            header:["id"],
            rows:[{"id":"1"}]
        })
    }),
    it('parses a table with 1 columns and 2 rows', () => {
        const table = 
`| id |
| 1 |
| 2 |`
        expect(parse(table)).toEqual({
            header:["id"],
            rows:[{"id":"1"},{"id":"2"}]
        })
    })
})